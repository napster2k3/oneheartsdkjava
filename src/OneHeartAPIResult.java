/**
 * OneHeart API Java SDK
 * @author Camille Greselle <cgreselle@oneheartcommunication.com> <camille.greselle@epitech.eu>
 */

import org.json.JSONObject;
import org.json.JSONArray;

public class OneHeartAPIResult {
    
    public boolean bStatus;
    public JSONArray dataArray;
    
    OneHeartAPIResult(String sResult) throws Exception {
        JSONObject resultJson = new JSONObject(sResult);
        this.bStatus = (boolean) resultJson.get("status");
        this.dataArray = (JSONArray) resultJson.get("datas");
        if(!this.bStatus && Oneheart_apiclient.getInstance().bDebug)
            throw new Exception("OneHeartAPIResult: False state");
    }
    
    public JSONObject getElement(int key) {
        System.out.println(this.dataArray.getJSONObject(key));
        return this.dataArray.getJSONObject(key);
    }
}
