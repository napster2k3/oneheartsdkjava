/**
 * OneHeart API Java SDK
 * @author Camille Greselle <cgreselle@oneheartcommunication.com> <camille.greselle@epitech.eu>
 */

import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;

abstract class OneHeartRequest {
    
    /** Relative API Information **/
    static final String OHC_API_URL = "http://www.oneheartcommunication.com/api/";
    static final String OHC_API_METHOD_GET = "GET";
    static final String OHC_API_METHOD_POST = "POST";
    static final String OHC_API_METHOD_PUT = "PUT";
    static final String OHC_API_METHOD_DELETE = "DELETE";
    
    public String ressource;
    
    OneHeartRequest(String ressource) {
        this.ressource = ressource;
    }
    
    /** Request API method **/
    
    public OneHeartAPIResult call(String sMethod, String sUri, String sParameters) throws Exception {
    
        String sParsedUrl = OneHeartRequest.OHC_API_URL + sUri + sParameters;
        URL url = new URL(sParsedUrl);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setRequestMethod(sMethod);
        request.setRequestProperty("Accept", "application/json");
        
        /** HTTP BASIC AUTH **/
        String sUserPassword = Oneheart_apiclient.getInstance().sClientId + ":" + Oneheart_apiclient.getInstance().sClientSecret;
        String sEncoding = new sun.misc.BASE64Encoder().encode(sUserPassword.getBytes());
        request.setRequestProperty("Authorization", "Basic " + sEncoding);
        
        try {
            this.isValidResponseCode(request.getResponseCode());
        } catch(Exception e) {
            if(Oneheart_apiclient.getInstance().bDebug)
                System.out.println(e);
        }
                
        // Get request content
        
        BufferedReader buffer = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String inputLine;
	StringBuffer response = new StringBuffer();
        
        while ((inputLine = buffer.readLine()) != null)
            response.append(inputLine);
        
	buffer.close();
        return new OneHeartAPIResult(response.toString());  
    }
    
    /** Check the server answer response is valid **/
    
    public boolean isValidResponseCode(int iResponseCode) throws Exception {
        
        switch(iResponseCode) {
            case 200 :
                return true;  
            case 401 : 
                throw new Exception("Make sure your client id and secret are correct");
        }
        
        return false;
    }
    
}
