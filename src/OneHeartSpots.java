public class OneHeartSpots extends OneHeartRequest {
    
    OneHeartSpots() {
        super("spots");
    }
    
    public OneHeartAPIResult getAll(String sParameters) throws Exception {
        return super.call(
                super.OHC_API_METHOD_GET,
                "spots",
                sParameters
        );
    }
    
    public OneHeartAPIResult getById(int iSpotId, String sParameters) throws Exception {
        return super.call(
                super.OHC_API_METHOD_GET,
                "spots/" + iSpotId,
                sParameters
        );
    }
}
