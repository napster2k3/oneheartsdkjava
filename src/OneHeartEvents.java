public class OneHeartEvents extends OneHeartRequest {
    
    OneHeartEvents() {
        super("events");
    }
    
    public OneHeartAPIResult getAll(String sParameters) throws Exception {
        return super.call(
                super.OHC_API_METHOD_GET,
                "events",
                sParameters
        );
    }
    
    public OneHeartAPIResult getById(int iEventId, String sParameters) throws Exception {
        return super.call(
                super.OHC_API_METHOD_GET,
                "events/" + iEventId,
                sParameters
        );
    }
}
