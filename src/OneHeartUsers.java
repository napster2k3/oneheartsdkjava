/**
 * OneHeart API Java SDK
 * @author Camille Greselle <cgreselle@oneheartcommunication.com> <camille.greselle@epitech.eu>
 */

public class OneHeartUsers extends OneHeartRequest {
    
    OneHeartUsers() {
        super("users");
    }
    
    public OneHeartAPIResult getAll(String sParameters) throws Exception {
        return super.call(
                super.OHC_API_METHOD_GET,
                "users",
                sParameters
        );
    }
    
    public OneHeartAPIResult getById(int iUserId, String sParameters) throws Exception {
        return super.call(
                super.OHC_API_METHOD_GET,
                "users/" + iUserId,
                sParameters
        );
    }
}
