
public class OneHeartVideos extends OneHeartRequest {
    
    OneHeartVideos() {
        super("videos");
    }
    
    public OneHeartAPIResult getAll(String sParameters) throws Exception {
        return super.call(
                super.OHC_API_METHOD_GET,
                "videos",
                sParameters
        );
    }
    
    public OneHeartAPIResult getById(int iVideoId, String sParameters) throws Exception {
        return super.call(
                super.OHC_API_METHOD_GET,
                "videos/" + iVideoId,
                sParameters
        );
    }
    
    public OneHeartAPIResult watchById(int iVideoId, String sParameters) throws Exception {
        return super.call(
                super.OHC_API_METHOD_POST,
                "videos/watch/" + iVideoId,
                sParameters
        );
    }
}
