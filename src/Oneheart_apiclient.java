/**
 * OneHeart API Java SDK
 * @author Camille Greselle <cgreselle@oneheartcommunication.com> <camille.greselle@epitech.eu>
 */

public class Oneheart_apiclient {
    
    /** API User Information **/
    public String sClientId;
    public String sClientSecret;
    public boolean bDebug;
    
    private Oneheart_apiclient(String clientId, String clientSecret, boolean debug) {
        this.sClientId = clientId;
        this.sClientSecret = clientSecret;
        this.bDebug = debug;
    }
    
    /** Singleton **/
    private static Oneheart_apiclient INSTANCE;
    
    /** INITIALIZE INSTANCE **/
    public static Oneheart_apiclient initInstance(String clientId, String clientSecret, boolean debug) throws Exception {
        if(INSTANCE != null)
            throw new Exception("Instance already initialized, use getInstance() to reach it");
        
        INSTANCE = new Oneheart_apiclient(clientId, clientSecret, debug);
        return INSTANCE;
    }
    
        
    /** get Singleton Instance **/
    public static Oneheart_apiclient getInstance() throws Exception {
        if(INSTANCE == null)
            throw new Exception("You must init the instance first.");
        
        return INSTANCE;
    }
    
    public static void dispose() throws Exception {
        if(INSTANCE == null)
            throw new Exception("no singleton to dispose");
        
        INSTANCE = null;
    }
    
    /** API Entity **/
    private OneHeartUsers Users;
    
    public OneHeartUsers getUsers() {
        if(this.Users == null)
            this.Users = new OneHeartUsers();
        
        return this.Users;
    }
    
    private OneHeartSpots Spots;
    
    public OneHeartSpots getSpots() {
        if(this.Spots == null)
            this.Spots = new OneHeartSpots();
        
        return this.Spots;
    }
    
    private OneHeartEvents Events;
    
    public OneHeartEvents getEvents() {
        if(this.Events == null)
            this.Events = new OneHeartEvents();
        
        return this.Events;
    }
    
    
}

// demo class

class run {
    public static void main(String[] args) { 
       try {
            Oneheart_apiclient.initInstance("cgreselle@oneheartcommunication.com", "z2Y8368jYM7gHf06Vb2C", true);
            String sParameters = "";
            String tmp = Oneheart_apiclient.getInstance()
                         .getUsers()
                         .getAll(sParameters)
                         .getElement(5)
                         .get("verified").toString();
        } catch(Exception e) {
            System.out.println(e);
        }
    }
}
